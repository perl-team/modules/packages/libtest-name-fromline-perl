#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2015 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for Test::Name::FromLine
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/cdbs/1/rules/upstream-tarball.mk
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/class/perl-build.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)

DEB_UPSTREAM_PACKAGE = Test-Name-FromLine
DEB_UPSTREAM_URL = http://www.cpan.org/modules/by-module/Test

# Build-depend unversioned on debhelper
#  TODO: Drop when adopted in cdbs
CDBS_BUILD_DEPENDS_rules_debhelper_v9 = debhelper

# Needed by upstream build
bdeps = perl (>= 5.13.10) | libcpan-meta-perl

# Needed by upstream testsuite
bdeps-test = libtest-differences-perl, libtest-fatal-perl

# Needed by upstream build and (always) at runtime
deps = libfile-slurp-perl

CDBS_BUILD_DEPENDS += , $(bdeps), $(bdeps-test), $(deps)
CDBS_DEPENDS_$(pkg) = $(deps)
